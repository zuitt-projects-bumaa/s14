
// alert("hi");
console.log(3+5);
console.log(3/0);

function checkDivisibility(dividend, divisor) {
	if (dividend % divisor == 0) {
		console.log(`${dividend} is divisible by ${divisor}`);
	} else {
		console.log(`${dividend} is not divisible by ${divisor}`);
	}
}

checkDivisibility(100,0);

let items = [
	{
		id: 1,
		name: 'Banana',
		description: 'A yellow fruit',
		price: 15.00, 
		category: 2
	},
		{
		id: 1,
		name: 'Pork',
		description: 'A yellow fruit',
		price: 15.00, 
		category: 2
	},	
	{
		id: 1,
		name: 'Potato',
		description: 'A yellow fruit',
		price: 15.00, 
		category: 2
	}
];

for (let i = 0; i < items.length; i++) {
	console.log(`
		Name: ${items[i].name}
		Description: ${items[i].description}
		Price: ${items[i].price}
		`);
}

/* 
JS- for interactive web pages

ways to add comments in JS:
		//			--single line comment
		ctrl shift /  --multi line comment
*/
console.log("Hello, World!");
/*
	console.log() allows us to show/display data in our console.
	The console is part of our browser with which we can use 
	to display data.
*/

/* mini activity*/

console.log("Emma");
console.log("Grumpy Joe Pizza");

/*
	Statements and Syntax

	Statement- instructions or expressions we add to our script/
		program which will be communicated to our computers.
		Our computers will then be able to interpret these instructions
		and perform the task accordingly.

		Most programming languages end their statements in semicolon (;)
		JavaScript has ASI (auto semicolon insertion). Better practice 
		is still to add semicolon.

	Syntax- set of rules that describes how statements are properly
		made or constructed
*/

// Variables are containers of data
let name = "Emmalyn Grace Camacho Buma-a"

//log the value of the console:
console.log(name);


let num = 5;
let num2 = 10;
console.log(num);
console.log(num2);

console.log (name,num,num2);

// We cannot display the vallue of a variable that is not yet created/declared
// console.log(name2);

// you can declare a variable with no value yet
// we can add value in the future
let myVariable;

// undefined
console.log(myVariable);

/*
	2 Steps in Creating Variables:
	1. Declaration
	2. Initialization
*/

myVariable = "new value"

console.log (myVariable);


let bestFinalFantasy = "Final Fantasy X";
console.log(bestFinalFantasy);

// You can update the value of a variable declared using the let keyword
bestFinalFantasy = "Final Fantasy 7"
console.log(bestFinalFantasy)

// we shouldn't update using the let keyword
// we cannot create another variable with the same name
// let bestFinalFantasy = "Final Fantasy 6"    ---error

/*
	Const

	const keyword allows us to craete a variable like let, however,
	the const variable cannot be updated. values in a const cannot be changed.
	You also cannot create a const variable without initialization.
*/

const pi = 3.1416;
console.log(pi);

// pi = 3.15;
// console.log(pi);   ---error


// const plateNum; 
// console.log(plateNum);   ---error: missing initializer in const declaration

let name2 = "Edward Cullen"; 
let role = "Supervisor";

/*
	mini activity
*/

role = "Director";
const tin = "12333-1234";
console.log(name2,role,tin);

/*
	Conventions in Variable/constant names
*/

let country = "Philippines";
let province = "Metro Manila";

console.log(province, country);

// CONCATENATION

let address = province + "," + country;
console.log(address);

// mini activity

let city1 = "Manila";
let city2 = "Copenhagen";
let city3 = "Washington D.C.";
let city4 = "Tokyo";
let city5 = "New York";
let country1 = "Philippines";
let country2 = "U.S.A.";
let country3 = "South Korea";
let country4 = "Japan";


let capital1 = city1 + ", " + country1;
let capital2 = city3 + ", " + country2;
let capital3 = city4 + ", " + country4;

console.log(capital1);
console.log(capital2);
console.log(capital3);

let number1 = 10;
let number2 = 6;

console.log(number1);
console.log(number2);

let sum1 = number1 + number2;
console.log(sum1);

let sum2 = 16 + 4;
console.log(sum2);

let numString1 = "30";
let numString2 = "50";

let sumString1 = numString1 + numString2;
console.log(sumString1);

let sum3 = number1 + numString2;
console.log(sum3);

//boolean

let isAdmin = true;
let isMarried = false;
let isMVP = true;

console.log("Is she married? " + isMarried);
console.log("Is Curry an MVP? " + isMVP);
console.log("Is he the current admin? " + isAdmin);

//array - for multiple values of the same date type
//     	- may be used for different data types, but not good practice

let array1 = ["Goku", "Gohan", "Goten", "Vegeta", "Trunks", "Broly"];
console.log(array1);


// object - contains different data types
//		- each field is called a property (key: value)

let hero1 = {
		heroName: "One Punch Man",
		realName: "Saitama",
		income: 5000, 
		isActive: true 
}

//mini-activity

let backstreetBoys = ["Brian", "Kevin", "Nick", "Howie", "AJ"];

let mbjae = {
		firstName: "MB Jae",	
		lastName: "Camacho",	
		isDeveloper: true,
		age: 35
}

console.log(backstreetBoys);
console.log(mbjae);

// undefined --not an error
let sampleVariable;

let person2 = {
		name: "Peter",
		age: 42
}

console.log(person2.name);
console.log(person2.age);

// undefined: person2 existe, but no such property
console.log(person2.isAdmin);

/*
		functions
		-lines/blocks of code that tell our device/ application to
		 perform a certain task
		 -program within a program
		 -syntax:
		 		function
*/ 

console.log("Good afternoon, everyone! Welcome to my application!");

function greet(){
	console.log("Good Afternoon, Everyone! Welcome to my application!");
}

// function invocation - call or use of function
greet();
greet();
greet();
greet();
greet();


// mini Activity

let favotiteFood = "pizza";
let sum4 = 150 + 9;
let product1 = 100*90;
let isActive = true

let favoriteRestaurants = ["Grumpy Joe", "Olives", "Sizzling Plate", "Don Henricos", "Kabsat"];

let mandyMoore = {
	firstName: "Amanda Leigh",
	lastName: "Moore",
	stageName: "Mandy Moore",
	birthDay: "April 10, 1984",
	age: "37",
	bestAlbum: "I Wanna Be With You",
	bestSong: "Only Hope",
	bestTvShow: null,
	bestMovie: "A Walk to Remember",
	isActive: false
}

console.log (favotiteFood); 
console.log (sum4);
console.log (product1); 
console.log (isActive); 
console.log (favoriteRestaurants); 
console.log (mandyMoore);



// discussion on Functions --Maam Tin

// Parameters and Arguments
// "name" is called a parameter
// parameter acts as a named variable/ container that exists ONLY in a function
// 


function printName(name){
	console.log(`My name is ${name}`)
};

//console.log(name);


//Data passed in the function: argument
// Representation of the argument within the function: parameter
printName("Jake");

function displayNum(number){
	console.log(number)
};

displayNum(3000);




// mini activity

function displayMessage(message){
	console.log(message)
};

displayMessage("JS is challenging!")


// Multiple Parameters and Arguments
/* 
Function can not only receive a single argument but in can also receive
multiple arguments as long as it matches the number of parameters in the function
*/

function displayFullName (firstName, lastName, age) {
	console.log(`${firstName} ${lastName} is ${age}`)
};

displayFullName("Emmalyn Grace", "Buma-a", "36");

//return keyword

function createFullName (firstName, middleName, lastName){
	return `${firstName} ${middleName} ${lastName}`
	console.log("I will no longer run because the function's value/result has been returned.")
}

let fullName1 = createFullName("Tom", "Cruise", "Mapother");

console.log(fullName1);








