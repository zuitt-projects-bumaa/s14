let fullName = "Steve Rogers";
console.log(fullName);

let userAge = "40";
console.log(userAge);

let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
console.log(friends);

let profile = {
	username: "captain_america",
	fullName: "Steve Rogers",
	age: "40",
	isActive: false
}

console.log(profile);

fullName = "Tony Stark";
console.log(fullName);

let largestOcean = "Pacific Ocean";
largestOcean = "Atlantic Ocean";
console.log(largestOcean);

function getSum (num1, num2) {
	return num1 + num2;
};

let sum = getSum(7, 3);

console.log(sum);

